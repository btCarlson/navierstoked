#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    //  OF Settings
    ofHideCursor();
    ofSetFrameRate(60);
    ofSetWindowShape(1280, 905);
    //ofSetWindowPosition(-1920, 20);
    
    //  Data
    w = ofGetWidth();
    h = ofGetHeight();
    
    resetFrame = 0;
    
    //  PingPong
    pingPong.allocate(w, h);
    outPong.allocate(w, h);
    numPasses = 1;
    
    //  Primitives
    plane.set(w, h, 10, 10);
    plane.mapTexCoords(0, 0, w, h);
    
    //  Shaders
    nsShaderPath = "shaders/navierStoked";
    nsShader.load(nsShaderPath);
    cShaderPath = "shaders/c";
    cShader.load(cShaderPath);
    
    //  Textures
    noiseImg.load("images/fractalNoise.jpg");
    sourceImg.load("images/OddCity.png");
    sourceImg.mirror(false, false);
    //sourceVideo.load("images/kick_01.mov");
    //sourceVideo.play();
    
    drawSourceImage();
}

//--------------------------------------------------------------
void ofApp::update(){

    //  Window Title
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
    //  Data
    t = ofGetElapsedTimef();
    f = ofGetFrameNum();
    
    //  Video
    sourceVideo.update();
    
    //  Navier Stokes
    ofEnableBlendMode(OF_BLENDMODE_DISABLED);
    for(int i = 0; i < numPasses; ++i)
    {
        //  PingPong
        pingPong.dst->begin();
        //ofClear(0, 0, 0, 255);
        
        //  Shaderr
        nsShader.begin();
        
        //nsShader.setUniform1i("frame", 1);
        nsShader.setUniform1i("frame", f - resetFrame);
        nsShader.setUniform2f("res", w, h);
        if(ofGetMousePressed())
            nsShader.setUniform4f("mouse", mouseX, h - mouseY, ofGetPreviousMouseX(), h - ofGetPreviousMouseY());
        else
            nsShader.setUniform4f("mouse", 0, 0, 0, 0);
        nsShader.setUniformTexture("iChannel0", pingPong.src->getTexture(), 0);
        nsShader.setUniformTexture("imgTex", sourceImg.getTexture(), 1);
        
        ofPushMatrix();
        ofTranslate(0.5 * w, 0.5 * h);
        plane.draw();
        ofPopMatrix();
        
        nsShader.end();
        
        //  PingPong
        pingPong.dst->end();
        pingPong.swap();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

    //  BG
    ofBackground(0);
    
    //  PingPong
    outPong.dst->begin();

    ofBackground(0);
    //  Visualize
    //  C Shader
    cShader.begin();

    cShader.setUniformTexture("ns", pingPong.src->getTexture(), 0);
    cShader.setUniformTexture("img", sourceImg.getTexture(), 1);
    cShader.setUniformTexture("prev", outPong.src->getTexture(), 2);

    ofPushMatrix();
    ofTranslate(0.5 * w, 0.5 * h);
    plane.draw();
    ofPopMatrix();

    cShader.end();
    
    outPong.dst->end();
    outPong.swap();
    
    //  Draw
    outPong.src->draw(0, 0);
}

//--------------------------------------------------------------
void ofApp::drawSourceImage(){
    outPong.src->begin();
    sourceImg.draw(0, 0);
    outPong.src->end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    switch(key)
    {
        case 'r':
            nsShader.unload();
            nsShader.load(nsShaderPath);
            cShader.unload();
            cShader.load(cShaderPath);
            
            resetFrame = f;
            
            pingPong.clear();
            outPong.clear();
            
            //drawSourceImage();
            break;
        default:
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
