#pragma once

#include "ofMain.h"
#include "PingPong.h"

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    
    void drawSourceImage();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
		
private:
    
    //  Data
    float w, h, t;
    int f, resetFrame;
    
    //  PingPong
    PingPong outPong;
    PingPong pingPong;
    int numPasses;
    
    //  Plane
    ofPlanePrimitive plane;
    
    //  Shaders
    string nsShaderPath;
    ofShader nsShader;
    string cShaderPath;
    ofShader cShader;
    
    //  Textures
    ofImage noiseImg;
    ofImage sourceImg;
    ofVideoPlayer sourceVideo;
};
