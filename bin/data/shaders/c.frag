#version 330

uniform sampler2DRect ns;
uniform sampler2DRect img;
uniform sampler2DRect prev;

in vec2 texCoordVarying;
out vec4 fragColor;

void main()
{
	vec4 g = texture(ns, gl_FragCoord.xy);
	vec4 bg = texture(img, gl_FragCoord.xy - 200.0 * vec2(g.rg)).rgba;
    vec3 color = g.w + abs(g.xyz) * vec3(10,10,.1);
    color = sin(color);
    color = texture(prev, gl_FragCoord.xy - 1.0 * vec2(g.rg)).rgb;


    fragColor = vec4(color, g.a);
    fragColor = mix(fragColor, bg, clamp(bg.a * 0.5, 0., 1.));
}