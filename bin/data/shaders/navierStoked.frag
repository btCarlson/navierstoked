#version 330


uniform int frame;
uniform vec2 res;
uniform vec4 mouse;
uniform sampler2DRect iChannel0;
uniform sampler2DRect imgTex;

in vec2 texCoordVarying;
out vec4 fragColor;

float hash(float n) { return fract(sin(n) * 1e4); }
float hash(vec2 p) { return fract(1e4 * sin(17.0 * p.x + p.y * 0.1) * (0.1 + abs(sin(p.y * 13.0 + p.x)))); }

float noise(float x) {
    float i = floor(x);
    float f = fract(x);
    float u = f * f * (3.0 - 2.0 * f);
    return mix(hash(i), hash(i + 1.0), u);
}

float noise(vec2 x) {
    vec2 i = floor(x);
    vec2 f = fract(x);

    // Four corners in 2D of a tile
    float a = hash(i);
    float b = hash(i + vec2(1.0, 0.0));
    float c = hash(i + vec2(0.0, 1.0));
    float d = hash(i + vec2(1.0, 1.0));

    // Simple 2D lerp using smoothstep envelope between the values.
    // return vec3(mix(mix(a, b, smoothstep(0.0, 1.0, f.x)),
    //          mix(c, d, smoothstep(0.0, 1.0, f.x)),
    //          smoothstep(0.0, 1.0, f.y)));

    // Same code, with the clamps in smoothstep and common subexpressions
    // optimized away.
    vec2 u = f * f * (3.0 - 2.0 * f);
    float ret = mix(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
    ret -= 0.5;
    return ret;
}

// FLUID PART


vec2 ur, U;


float ln(vec2 p, vec2 a, vec2 b)
{
    return length(p - a - (b - a) * clamp(dot(p - a, b - a) / dot(b - a, b - a), 0.0, 1.0));
}


vec4 t (vec2 v, int a, int b)
{
	return texture(iChannel0, v + vec2(a, b));
}


vec4 t (vec2 v)
{
	return texture(iChannel0, v);
}


float area (vec2 a, vec2 b, vec2 c)
{	// area formula of a triangle from edge lengths
    float A = length(b-c),
    	  B = length(c-a),
    	  C = length(a-b),
          s = 0.5*(A+B+C);
    return sqrt(s * (s - A) * (s - B) * (s - C));
}


vec2 perp (vec2 v)
{
	return vec2(-v.y,v.x);
}


void main()
{
    U  = gl_FragCoord.xy;
    ur = res.xy;

    if (frame < 6)
    {
        U = 256. * (U - 0.5 * ur) / ur.y;
        float q = length(U);
        float ra = exp(-0.1e-3 * q * q);
        fragColor = 0.1 * vec4(1.0 * ra * vec2(sin(U.y), sin(U.x)), 0, 10.*sin(0.1*U.x));
        fragColor.rgb += -0.15 + 0.3 * texture(imgTex, gl_FragCoord.xy).rgb;
        fragColor.a += texture(imgTex, gl_FragCoord.xy).a;
        return;
    }
    else
    {
        vec2 v = U,
             A = v + vec2( 1, 1),
             B = v + vec2( 1,-1),
             C = v + vec2(-1, 1),
             D = v + vec2(-1,-1);

        for (int i = 0; i < 4; i++) {
            vec2 tmp = t(v).xy;
            v -= tmp;
        }

        vec4 me = t(v);
        for (int i = 0; i < 4; i++) {
            vec2 tmp = t(v).xy;
            v -= tmp;
        }

        me.zw = t(v).zw;

        for (int i = 0; i < 8; i++) {
            A -= t(A).xy;
            B -= t(B).xy;
            C -= t(C).xy;
            D -= t(D).xy;
        }

        vec4 n = t(v,0,1),
            e = t(v,1,0),
            s = t(v,0,-1),
            w = t(v,-1,0);

        vec4 ne = .25*(n+e+s+w);
        me = mix(me,ne,vec4(0.18,0.18,1.0,0.5));
        me.z  = me.z - (area(A,B,C)+area(B,C,D)-4.);
        vec4 pr = vec4(e.z,w.z,n.z,s.z);
        me.xy = me.xy + vec2(pr.x-pr.y, pr.z-pr.w)/ur;
        
        
        //  Mouse
        float q = ln(U,mouse.xy,mouse.zw);
        vec2 m = mouse.xy-mouse.zw;
        float l = length(m);
        if (l>0.) {
            m = normalize(m)*min(l,.1);
            me += exp(-q*q)*vec4(normalize(m.xy),0,1);
        }
        fragColor = me;
        fragColor.xyz = clamp(fragColor.xyz, -40., 40.);        
    }
}